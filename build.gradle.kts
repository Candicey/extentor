plugins {
    kotlin("jvm") version "1.9.21"
    kotlin("plugin.serialization") version "1.8.10"
}

group = "com.gitlab.candicey.extentor"
version = "0.1.0"

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))

    implementation(libs.bundles.asm)
    implementation(libs.kxSer)
    implementation(libs.ktRflct)
    implementation(libs.ktBytecodeDsl)
    implementation(libs.deencapsulation)
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

val agent by tasks.registering(Jar::class) {
    archiveAppendix.set("Agent")
    group = "build"

    from(sourceSets.main.get().output)
    from({ configurations.runtimeClasspath.get().map { zipTree(it) } }) {
        exclude("**/module-info.class")
    }

    manifest.attributes(
        "Premain-Class" to "com.gitlab.candicey.extentor.ExtentorKt",
        "Agent-Class" to "com.gitlab.candicey.extentor.ExtentorKt",
        "Can-Retransform-Classes" to "true",
    )
}

tasks {
    build {
        dependsOn(agent)
    }
}
