package com.gitlab.candicey.extentor.util

class ExtentorArrayList<E> : ArrayList<E>() {
    val addList = mutableSetOf<E>()

    fun addBypass(element: E) = super.add(element)

    fun addAllBypass(elements: Collection<E>) = super.addAll(elements)

    override fun add(element: E): Boolean = addList.add(element)

    /**
     * @param index ignored
     */
    override fun add(index: Int, element: E) {
        addList.add(element)
    }

    override fun addAll(elements: Collection<E>): Boolean = addList.addAll(elements)

    /**
     * @param index ignored
     */
    override fun addAll(index: Int, elements: Collection<E>): Boolean = addList.addAll(elements)

    override fun remove(element: E): Boolean = throw UnsupportedOperationException()

    override fun removeAll(elements: Collection<E>): Boolean = throw UnsupportedOperationException()

    override fun removeAt(index: Int): E = throw UnsupportedOperationException()

    override fun retainAll(elements: Collection<E>): Boolean = throw UnsupportedOperationException()

    override fun clear() = throw UnsupportedOperationException()

    override fun set(index: Int, element: E): E = throw UnsupportedOperationException()

    override fun removeRange(fromIndex: Int, toIndex: Int) = throw UnsupportedOperationException()

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<E> = throw UnsupportedOperationException()
}