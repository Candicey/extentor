package com.gitlab.candicey.extentor.util

import com.gitlab.candicey.extentor.extension.descriptor
import com.gitlab.candicey.extentor.node.OriginalDescriptor
import io.github.nilsen84.bytecode_dsl.InsnBuilder
import io.github.nilsen84.bytecode_dsl.asm
import org.objectweb.asm.Opcodes.INVOKESTATIC
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.MethodInsnNode

fun asmWithCompanion(block: InsnBuilder.() -> Unit): InsnList =
    asm(block)
        .also { insnList ->
            insnList
                .filterIsInstance<MethodInsnNode>()
                .forEach { methodInsnNode ->
                    val companion = companionClasses[methodInsnNode.owner]
                        ?.flatMap { it.declaredMethods.toList() }
                        ?.firstOrNull { it.name == methodInsnNode.name && it.getAnnotation(OriginalDescriptor::class.java)!!.descriptor == methodInsnNode.desc }
                        ?: return@forEach

                    methodInsnNode.apply {
                        owner = companion.declaringClass.name.replace('.', '/')
                        desc = companion.descriptor
                        opcode = INVOKESTATIC
                    }
                }
        }