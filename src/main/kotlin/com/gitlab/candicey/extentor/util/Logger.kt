package com.gitlab.candicey.extentor.util

internal fun info(message: String) = println("[INFO] $message")

internal fun warn(message: String) = println("[WARN] $message")

internal fun error(message: String) = println("[ERROR] $message")

