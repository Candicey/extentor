package com.gitlab.candicey.extentor.util

import org.objectweb.asm.Type
import java.util.*

val random = Random()

val companionClasses = mutableMapOf<String, MutableList<Class<*>>>()

fun ClassLoader.defineClass(name: String, bytes: ByteArray): Class<*> = reflectMethod("defineClass", String::class.java, ByteArray::class.java, Int::class.java, Int::class.java)(this, name, bytes, 0, bytes.size) as Class<*>

inline fun <reified T : Any> internalNameOf(): String = Type.getInternalName(T::class.java)