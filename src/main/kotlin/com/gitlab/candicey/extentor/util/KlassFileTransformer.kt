package com.gitlab.candicey.extentor.util

import java.lang.instrument.ClassFileTransformer
import java.security.ProtectionDomain

interface KlassFileTransformer : ClassFileTransformer {
    override fun transform(
        loader: ClassLoader,
        className: String,
        classBeingRedefined: Class<*>?,
        protectionDomain: ProtectionDomain,
        classfileBuffer: ByteArray
    ): ByteArray?
}