package com.gitlab.candicey.extentor

import MyClass
import com.gitlab.candicey.extentor.extension.*
import com.gitlab.candicey.extentor.node.ExtendedClassNode
import com.gitlab.candicey.extentor.util.*
import dev.xdark.deencapsulation.Deencapsulation
import io.github.nilsen84.bytecode_dsl.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodNode
import java.lang.instrument.Instrumentation
import java.security.ProtectionDomain

lateinit var inst: Instrumentation

fun premain(arg: String?, inst: Instrumentation) {
    com.gitlab.candicey.extentor.inst = inst
}

fun agentmain(arg: String?, inst: Instrumentation) {
    com.gitlab.candicey.extentor.inst = inst
}

fun main(args: Array<String>) {
    Deencapsulation.deencapsulate(ClassLoader::class.java)
    MyClass().eat()
    inst.addTransformer(Transformer, true)
    inst.retransformClasses(MyClass::class.java)

    val classNode = ClassNode().apply {
        name = "TestClass"
        access = Opcodes.ACC_PUBLIC
        superName = "java/lang/Object"
        version = Opcodes.V1_8
        methods = listOf(
            MethodNode(
                Opcodes.ACC_PUBLIC or Opcodes.ACC_STATIC,
                "run",
                "()V",
                null,
                null
            ).apply {
                instructions = asmWithCompanion {
                    new("MyClass")
                    dup
                    invokespecial("MyClass", "<init>", "()V")
                    dup
                    invokevirtual("MyClass", "eat", "()V")
                    invokevirtual("MyClass", "test", "()V") // generated method
                    ldc("Hello from generated test!")
                    invokestatic("MyClass", "testStatic", "(Ljava/lang/String;)V") // generated static method
                    _return
                }
            }
        )
    }
    Transformer::class.java.classLoader.defineClass("TestClass", classNode.toByteArray())

    val testClass = Class.forName("TestClass")
    reflectMethod(testClass, "run")(null)
}

object Transformer : KlassFileTransformer {
    override fun transform(
        loader: ClassLoader,
        className: String,
        classBeingRedefined: Class<*>?,
        protectionDomain: ProtectionDomain,
        classfileBuffer: ByteArray
    ): ByteArray? {
        if (className != "MyClass") {
            return null
        }
        println("Transforming $className")

        val (_, classNode) = classfileBuffer.parse()

        val extendedClassNode = ExtendedClassNode(classNode, loader)

        // add "test" method with no parameters
        extendedClassNode.methods.add(MethodNode(
            Opcodes.ACC_PUBLIC,
            "test",
            "()V",
            null,
            null
        ).apply {
            instructions = try {
                asm {
                    ldc("Hello from generated method!")
                    invokestatic<Transformer>(::print)
                    _return
                }
            } catch (e: Exception) {
                e.printStackTrace()
                throw e
            }
        })

        // add static "testStatic" method with one String parameter
        extendedClassNode.methods.add(MethodNode(
            Opcodes.ACC_PUBLIC or Opcodes.ACC_STATIC,
            "testStatic",
            "(Ljava/lang/String;)V",
            null,
            null
        ).apply {
            instructions = asm {
                aload(0)
                invokestatic<Transformer>(::print)
                _return
            }
        })

        extendedClassNode.processMethods()

        return extendedClassNode.toByteArray()
    }

    @JvmStatic
    fun print(message: String) {
        println(message)
    }
}