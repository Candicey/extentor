package com.gitlab.candicey.extentor.extension

import kotlin.reflect.KProperty

val KProperty<*>.descriptor: String
    get() = returnType.classDescriptor