package com.gitlab.candicey.extentor.extension

import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.Type
import org.objectweb.asm.tree.MethodNode

val MethodNode.isStatic: Boolean
    get() = access and ACC_STATIC != 0

val MethodNode.isFinal: Boolean
    get() = access and ACC_FINAL != 0

val MethodNode.isAbstract: Boolean
    get() = access and ACC_ABSTRACT != 0

val MethodNode.isSynchronized: Boolean
    get() = access and ACC_SYNCHRONIZED != 0

/**
 * Gets the parameters' descriptors of this method as a list of strings.
 *
 * @return The list of parameters' descriptors.
 */
val MethodNode.parametersString: List<String>?
    get() = desc?.let { string -> Type.getArgumentTypes(string).map { it.descriptor } }