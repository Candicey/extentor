package com.gitlab.candicey.extentor.extension

import com.gitlab.candicey.extentor.util.reflectMethod
import kotlin.reflect.KClass
import kotlin.reflect.KType

val KType.classDescriptor: String
    get() {
        val classifier = classifier as? KClass<*>
        return if (classifier == null) {
            "L${reflectMethod("getType")(this).toString().replace('.', '/')};"
        } else {
            when (classifier.java) {
                Array::class.javaObjectType -> "[${arguments[0].type!!.classDescriptor}"
                Boolean::class.javaPrimitiveType -> "Z"
                Char::class.javaPrimitiveType -> "C"
                Byte::class.javaPrimitiveType -> "B"
                Short::class.javaPrimitiveType -> "S"
                Int::class.javaPrimitiveType -> "I"
                Float::class.javaPrimitiveType -> "F"
                Long::class.javaPrimitiveType -> "J"
                Double::class.javaPrimitiveType -> "D"
                Void::class.javaPrimitiveType -> "V"
                else -> if (classifier.java.isArray) classifier.java.name.replace(
                    '.',
                    '/'
                ) else "L${classifier.java.name.replace('.', '/')};"
            }
        }

    }

/**
 * If the return type is Unit, return `V` instead of `Lkotlin/Unit;`.
 */
val KType.classDescriptorReturnType: String
    get() = if (classDescriptor == "Lkotlin/Unit;") "V" else classDescriptor