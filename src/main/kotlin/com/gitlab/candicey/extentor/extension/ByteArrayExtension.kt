package com.gitlab.candicey.extentor.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ByteArray.parse(): Pair<ClassReader, ClassNode> {
    val classReader = ClassReader(this)
    val classNode = ClassNode()
    classReader.accept(classNode, 0)
    return classReader to classNode
}