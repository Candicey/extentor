package com.gitlab.candicey.extentor.extension

import kotlin.reflect.KFunction

val KFunction<*>.descriptor: String
    get() = buildString {
        append("(")
        for (kParameter in parameters) {
            append(kParameter.type.classDescriptor)
        }
        append(")")
        append(returnType.classDescriptorReturnType)
    }