package com.gitlab.candicey.extentor.extension

import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.ClassNode
import java.io.File

val ClassNode.isInterface: Boolean
    get() = access and ACC_INTERFACE != 0

val ClassNode.isAbstract: Boolean
    get() = access and ACC_ABSTRACT != 0

val ClassNode.isFinal: Boolean
    get() = access and ACC_FINAL != 0

fun ClassNode.toByteArray(flags: Int = ClassWriter.COMPUTE_MAXS): ByteArray {
    val classWriter = ClassWriter(flags)
    accept(classWriter)
    return classWriter.toByteArray()
}

fun ClassNode.dump(path: String) = File(path).also { it.parentFile.mkdirs() }.writeBytes(toByteArray())