package com.gitlab.candicey.extentor.extension

import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.InsnList

val InsnList.readableString: String
    get() = joinToString("\n") { it.readableString }