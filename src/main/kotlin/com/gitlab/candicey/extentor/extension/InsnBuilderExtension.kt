package com.gitlab.candicey.extentor.extension

import com.gitlab.candicey.extentor.util.internalNameOf
import io.github.nilsen84.bytecode_dsl.InsnBuilder
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty

inline fun <reified T : Any> InsnBuilder.invokevirtual(method: KFunction<*>) = invokevirtual(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokestatic(method: KFunction<*>) = invokestatic(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokespecial(method: KFunction<*>) = invokespecial(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokeinterface(method: KFunction<*>) = invokeinterface(internalNameOf<T>(), method.name, method.descriptor)

inline fun <reified T : Any> InsnBuilder.getstatic(field: KProperty<*>) = getstatic(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.putstatic(field: KProperty<*>) = putstatic(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.getfield(field: KProperty<*>) = getfield(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.putfield(field: KProperty<*>) = putfield(internalNameOf<T>(), field.name, field.descriptor)