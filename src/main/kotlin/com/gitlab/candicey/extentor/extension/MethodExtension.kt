package com.gitlab.candicey.extentor.extension

import java.lang.reflect.Method

/**
 * Gets the descriptor of a method.
 *
 * @return The descriptor of the method.
 * @see [Class.toDescriptor]
 */
val Method.descriptor: String
    get() {
        val parameterTypes = parameterTypes
        val parameterTypeDescriptors = parameterTypes.joinToString("") { it.descriptor }
        val returnTypeDescriptor = returnType.descriptor
        return "($parameterTypeDescriptors)$returnTypeDescriptor"
    }