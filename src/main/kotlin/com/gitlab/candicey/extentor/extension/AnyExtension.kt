package com.gitlab.candicey.extentor.extension

import com.gitlab.candicey.extentor.util.reflectField

fun <T> T.copyTo(target: T) =
    this!!::class.java.allFields.forEach {
        runCatching { target!!.reflectField(it.name)[target] = it.also { it.isAccessible = true }[this] }
    }