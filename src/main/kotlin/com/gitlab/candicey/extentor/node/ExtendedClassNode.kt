package com.gitlab.candicey.extentor.node

import com.gitlab.candicey.extentor.extension.*
import com.gitlab.candicey.extentor.util.*
import org.objectweb.asm.*
import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodNode
import kotlin.collections.getOrPut

class ExtendedClassNode(val original: ClassNode, val classLoader: ClassLoader) : ClassNode(ASM9) {
    private val extendedMethods: ExtentorArrayList<MethodNode>
        get() = methods as ExtentorArrayList<MethodNode>

    init {
        original.copyTo(this)
        methods = ExtentorArrayList<MethodNode>().apply { addAllBypass(methods) }
    }

    fun processMethods() {
        val generatedClass = ClassNode().apply {
            name = "com/gitlab/candicey/extentor/generated/${original.name}_Generated_${System.currentTimeMillis()}_${random.nextInt().toUInt()}"
            access = ACC_PUBLIC or ACC_FINAL
            superName = "java/lang/Object"
            version = V1_8
        }

        for (addMethod in extendedMethods.addList) {
            val wrappedMethod = if (addMethod.isStatic) {
                MethodNode(
                    ACC_PUBLIC or ACC_STATIC or ACC_FINAL or (if (addMethod.isSynchronized) ACC_SYNCHRONIZED else 0),
                    addMethod.name,
                    addMethod.desc,
                    null,
                    null
                ).apply { instructions = addMethod.instructions }
            } else {
                MethodNode(
                    ACC_PUBLIC or ACC_STATIC or ACC_FINAL or (if (addMethod.isSynchronized) ACC_SYNCHRONIZED else 0),
                    addMethod.name,
                    "(L${original.name};${addMethod.desc.substring(1)}",
                    null,
                    null
                ).apply { instructions = addMethod.instructions }
            }.apply {
                visibleAnnotations = listOf(
                    AnnotationNode("L${internalNameOf<OriginalDescriptor>()};").apply {
                        values = listOf(
                            "descriptor",
                            addMethod.desc
                        )
                    },
                )
            }

            generatedClass.methods.add(wrappedMethod)
        }

        val bytes = generatedClass.toByteArray()

        val companionClass = classLoader.defineClass(generatedClass.name.replace('/', '.'), bytes)

        companionClasses.getOrPut(original.name) { mutableListOf() }.add(companionClass)
    }

    override fun getDelegate(): ClassVisitor = original.delegate

    override fun visit(version: Int, access: Int, name: String?, signature: String?, superName: String?, interfaces: Array<out String>?) = original.visit(version, access, name, signature, superName, interfaces)

    override fun visitSource(file: String?, debug: String?) = original.visitSource(file, debug)

    override fun visitModule(name: String?, access: Int, version: String?): ModuleVisitor = original.visitModule(name, access, version)

    override fun visitOuterClass(owner: String?, name: String?, desc: String?) = original.visitOuterClass(owner, name, desc)

    override fun visitAnnotation(desc: String?, visible: Boolean) = original.visitAnnotation(desc, visible)

    override fun visitTypeAnnotation(typeRef: Int, typePath: TypePath?, desc: String?, visible: Boolean) = original.visitTypeAnnotation(typeRef, typePath, desc, visible)

    override fun visitAttribute(attr: Attribute?) = original.visitAttribute(attr)

    override fun visitInnerClass(name: String?, outerName: String?, innerName: String?, access: Int) = original.visitInnerClass(name, outerName, innerName, access)

    override fun visitField(access: Int, name: String?, desc: String?, signature: String?, value: Any?) = original.visitField(access, name, desc, signature, value)

    override fun visitMethod(access: Int, name: String?, desc: String?, signature: String?, exceptions: Array<out String>?) = original.visitMethod(access, name, desc, signature, exceptions)

    override fun visitNestHost(nestHost: String?) = original.visitNestHost(nestHost)

    override fun visitNestMember(nestMember: String?) = original.visitNestMember(nestMember)

    override fun visitPermittedSubclass(permittedSubclass: String?) = original.visitPermittedSubclass(permittedSubclass)

    override fun visitRecordComponent(name: String?, descriptor: String?, signature: String?): RecordComponentVisitor = original.visitRecordComponent(name, descriptor, signature)

    override fun check(api: Int) = original.check(api)

    override fun accept(classVisitor: ClassVisitor?) = original.accept(classVisitor)

    override fun visitEnd() = original.visitEnd()
}