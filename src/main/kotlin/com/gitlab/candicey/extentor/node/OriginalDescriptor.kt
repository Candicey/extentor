package com.gitlab.candicey.extentor.node

annotation class OriginalDescriptor(val descriptor: String)
